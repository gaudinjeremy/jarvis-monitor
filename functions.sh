#!/bin/bash

#MODULE MONITOR

js_mr_getTemp(){

    temp=$(cat /sys/class/thermal/thermal_zone0/temp)
    temp=$(($temp/1000))
	echo $temp
}

js_mr_getMemory(){

	memory=$(free -h)
    total=$(echo $memory | awk -F' ' '{print $8}')
	total=$(echo $total | sed 's/M//g')
    used=$(echo $memory | awk -F' ' '{print $9}')
	used=$(echo $used | sed 's/M//g')
	percentUsed=$((($used * 100) / $total))
    free=$(echo $memory | awk -F' ' '{print $10}')
	free=$(echo $free | sed 's/M//g')
	percentFree=$((($free * 100) / $total))

	if [ $1 = "total" ]
	then
		echo $total
	elif [ $1 = "used" ]
	then
		echo $used
	elif [ $1 = "percentused" ]
	then
		echo $percentUsed
	elif [ $1 = "free" ]
	then
		echo $free
	elif [ $1 = "percentfree" ]
	then
		echo $percentFree
	else
		echo $total"/"$used"/"$percentUsed"/"$free"/"$percentFree
	fi
}

js_mr_getDisk(){

	disk=$(df -h)
	total=$(echo $disk | awk -F' ' '{print $11}')
	used=$(echo $disk | awk -F' ' '{print $12}')
	percentUsed=$(echo $disk | awk -F' ' '{print $14}')
	percentUsed=$(echo $percentUsed | sed 's/%//g')
	free=$(echo $disk | awk -F' ' '{print $13}')
	percentFree=$((100 - $percentUsed))

	if [ $1 = "total" ]
    then
        echo $total
    elif [ $1 = "used" ]
    then
        echo $used
	elif [ $1 = "percentused" ]
	then
		echo $percentUsed
    elif [ $1 = "free" ]
    then
        echo $free
	elif [ $1 = "percentfree" ]
	then
		echo $percentFree
    else
        echo $total"/"$used"/"$percentUsed"/"$free"/"$percentFree
    fi
}

js_mr_getCpu(){

	cpu=$(vmstat 2 3 | tail -n1 | sed "s/\ \ */\ /g" | cut -d' ' -f 16)
  	cpu=$((100 - cpu))
	echo $cpu
}

js_mr_setMaxValue(){

    old=$(js_ia_getStrictText $1 2)
    js_ia_upCell $old $2 $1
    js_ia_say "confirmation"
}

js_mr_etatSystem(){

	temp=$(js_mr_getTemp)
	say "Température à $temp degrés"
    memory=$(js_mr_getMemory percentused)
	say "Mémoire vive utilisé à $memory pourcent"
    disk=$(js_mr_getDisk percentused)
	say "Espace disque utilisé à $disk pourcent"
    cpu=$(js_mr_getCpu)
	say "Cpu utilisé à $cpu pourcent"
}

js_mr_howAreYou(){

	state="0"
	temp=$(js_mr_getTemp)
	memory=$(js_mr_getMemory percentused)
	disk=$(js_mr_getDisk percentused)
	cpu=$(js_mr_getCpu)

    sendTemp="false"
    sendMemory="false"
    sendDisk="false"
    sendCpu="false"

    maxTemp=$(js_ia_getStrictText maxtemp 2)
    if [ $temp -ge $maxTemp ] && [ $temp -lt 70 ]
    then
        state=$(($state + 1))
        sendTemp="true"
        msgTemp="Ma température est à $temp degrés"

    elif [ $temp -ge 70 ] && [ $temp -lt 75 ]
    then
        state=$(($state + 2))
        sendTemp="true"
        msgTemp="Ma température est à $temp degrés"

    elif [ $temp -ge 75 ]
    then
        state=$(($state + 3))
        sendTemp="true"
        msgTemp="Ma température est à $temp degrés"
    fi

    maxMemory=$(js_ia_getStrictText maxmemory 2)
	if [ $memory -ge $maxMemory ]
    then
        state=$(($state + 1))
        sendMemory="true"
        msgMemory="Ma mémoire vive est utilisée est à $memory %"
    fi

    maxDisk=$(js_ia_getStrictText maxdisk 2)
	if [ $disk -ge $maxDisk ]
    then
        state=$(($state + 1))
        sendDisk="true"
        msgDisk="Mon espace disque est utilisé est à $disk %"
    fi

    maxCpu=$(js_ia_getStrictText maxcpu 2)
	if [ $cpu -ge $maxCpu ]
    then
        state=$(($state + 1))
        sendCpu="true"
        msgCpu="Mon CPU est utilisé est à $cpu %"
    fi


	if [ $state = 0 ]
    then
        msg="Tout vas bien"

    elif [ $state = 1 ]
    then
        msg="Quelque chose ne vas pas"

    elif [ $state = 2 ]
    then
        msg="Ca pourai allez mieux"

    elif [ $state = 3 ]
    then
        msg="Ca ne va pas, mon système est mis à mal"

    elif [ $state = 4 ]
    then
        msg="Ca va très mal ! je préconise un redémarage système"

	elif [ $state -ge 4 ]
	then
		msg="Mon système est compromis, il faut m'éteindre et faire un état système"
    fi


    if [ $1 = 'false' ]
    then
        say "$msg"
    else
        if [ $state != 0 ]
        then
            js_ia_sendToMe "$msg"
        fi
    fi


    if [ $sendTemp = 'true' ]
    then
        if [ $1 = 'false' ]
        then
            say "$msgTemp"
        else
            js_ia_sendToMe "$msgTemp"
        fi
    fi

    if [ $sendMemory = 'true' ]
    then
        if [ $1 = 'false' ]
        then
            say "$msgMemory"
        else
            js_ia_sendToMe "$msgMemory"
        fi
    fi

    if [ $sendDisk = 'true' ]
    then
        if [ $1 = 'false' ]
        then
            say "$msgDisk"
        else
            js_ia_sendToMe "$msgDisk"
        fi
    fi

    if [ $sendCpu = 'true' ]
    then
        if [ $1 = 'false' ]
        then
            say "$msgCpu"
        else
            js_ia_sendToMe "$msgCpu"
        fi
    fi
}
